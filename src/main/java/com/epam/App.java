package com.epam;

import com.epam.homework.Car;
import com.epam.homework.MyAnnotation;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class App {
    public static void main(String[] args) {
        Class cl = Car.class;
        Class c2 = Car.class;
        Car car = new Car();
        Field[] fields = cl.getDeclaredFields();
        Method[] methods = c2.getMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
        }
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);
                int wh = myAnnotation.wheeles();
                String color = myAnnotation.color();
                System.out.println("field " + field.getName() + " !");
                System.out.println("  name in @MyAnnotation "+ wh);
                System.out.println("  name in @MyAnnotation "+ color);
            }
        }

    }
}
