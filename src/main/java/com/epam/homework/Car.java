package com.epam.homework;

public class Car {
    @MyAnnotation
    private int wheeles;
    @MyAnnotation(color = "red")
    private String color;
    private int speed = 180;
    @MyAnnotation(type = "comfort")
    public String type;

    public int getSpeed() {
        return speed;
    }

    public int getWheeles() {
        return wheeles;
    }

    public String getColor() {
        return color;
    }

    public String getType() {
        return type;
    }
}
